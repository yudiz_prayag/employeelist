import { Component, OnInit} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/provider/auth.service';
import { MatDialog } from "@angular/material/dialog";
import { AddEmployComponent } from './add-employ/add-employ.component';
import { AuthDataManegerService } from 'src/app/provider/auth-data-maneger.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { EditEmployComponent } from './edit-employ/edit-employ.component';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  
  fullname = "Employee list"
  employ_list: any = []
  respons = true
  loginUser:any=[]
  
  // employ_name: any = []

  constructor(private _authService: AuthService,
    private router: Router,
    public bd: AngularFireDatabase,
    public dialog: MatDialog,
    private _authDataManeger: AuthDataManegerService,
  ) {
    const userdata = JSON.parse(localStorage.getItem("user")!)
    this.loginUser = userdata
  }
 
  

  ngOnInit(): void {

    this._authDataManeger.getdata().subscribe(data => {

      // console.log(data)
      this.employ_list = data
      this.respons = false
    })

 
    
  }




  addEmploy() {
    let ref = this.dialog.open(AddEmployComponent, { width: '500px', height: '500px' });
    
  }

  viewProfile(obj:any) {
    console.log("working")
    this.dialog.open(ViewProfileComponent,{ width: '400px', height: '450px' ,
    data:obj
  });

  }

  delete_E(id:any) {
    console.log(id)
  this._authDataManeger.delete(id)
  }

  edit_E(obj:any){

    this.dialog.open(EditEmployComponent, { width: '500px', height: '500px',
      data:obj
    })
  }

  showPhoto(){
    console.log("photo show")
  }


}

