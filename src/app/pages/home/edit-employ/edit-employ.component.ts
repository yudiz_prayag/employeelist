import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthDataManegerService } from 'src/app/provider/auth-data-maneger.service';

@Component({
  selector: 'app-edit-employ',
  templateUrl: './edit-employ.component.html',
  styleUrls: ['./edit-employ.component.scss']
})
export class EditEmployComponent implements OnInit {

  Editform!:FormGroup
  constructor(
    private fb:FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private storage: AngularFireStorage,
    private _authDatamanger:AuthDataManegerService

  ) {
    console.log(data)
   }

  ngOnInit(): void {
    this.Editform = this.fb.group({
      E_Name:[this.data.E_Name],
      E_Email:[this.data.E_Email],
      E_Address:[this.data.E_Address],
      E_Number:[this.data.E_Number],
      E_Department:[this.data.E_Department],
      E_Status:[this.data.E_Status],
      E_id:[this.data.E_id],
      E_Photo:[this.data.E_Photo] //no file visible
    })
    
  }

  edit(){
    this._authDatamanger.update(this.data.id,this.Editform.value)
  }

  uploadFile(e:any){
    const file = e.target!.files[0];
    console.log(file)
    const filePath =`images/${file.name}`;
    this.E_Photo.setValue(filePath)
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);
  
  }

  get E_Photo(){
    return this.Editform.controls['E_Photo']
  }
}
