import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/provider/auth.service';

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})
export class ForgetComponent implements OnInit {

  forgetForm!:FormGroup
  constructor(
    private fb:FormBuilder,
    private _authService:AuthService
    ) { }

  ngOnInit(): void {
    this.forgetForm = this.fb.group({
      forget_Email:["",[Validators.email,Validators.required]],
      checkbox:["",[Validators.required]]
    })
  }

  forgetRequest(){
    this._authService.forgetPassword(this.forgetForm.value.forget_Email)
  }
}
