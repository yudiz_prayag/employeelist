import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/provider/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  
  @Input("name") name!:string

  loginUser:any=[]
  
  constructor(private router:Router,
    private _authService:AuthService) { 
     const userdata = JSON.parse(localStorage.getItem("user")!)
     this.loginUser = userdata
  }
  ngOnInit(): void {
  }
  
  async logout(){
    try {
      const result_1 = await this._authService.signout();
      this.router.navigateByUrl("/login");
      localStorage.removeItem("user")
    } catch (err) {
      console.log(err);
    }
  }


}
