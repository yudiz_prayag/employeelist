import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthDataManegerService } from 'src/app/provider/auth-data-maneger.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-add-employ',
  templateUrl: './add-employ.component.html',
  styleUrls: ['./add-employ.component.scss']
})

export class AddEmployComponent implements OnInit {

  formEmploy!:FormGroup

  constructor( private fb : FormBuilder,
    private _authDataManger:AuthDataManegerService,
    private storage: AngularFireStorage,
  ) {}

  ngOnInit(): void {
    this.formEmploy = this.fb.group({
      E_Name:["",[Validators.required]],
      E_Email:["",[Validators.required,Validators.email]],
      E_Address:["",[Validators.required]],
      E_Number:["",[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      E_Department:["",[Validators.required]],
      E_Status:["",[Validators.required]],
      E_id:["",[Validators.required]],
     E_Photo:[""],
    // 
    })

  }
  
  add(){
    this._authDataManger.setData(this.formEmploy.value).then((result) => {
    // this.E_id.setValue(result.id)
    }).catch((err) => {
      
    });
    // this._authDataManger.setData()
  }
  
  uploadFile(e:any){
    const file = e.target!.files[0];
    console.log(file)
    const filePath =`images/${file.name}`;
    this.E_Photo.setValue(filePath)
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);
  
  }

  get E_Photo(){
    return this.formEmploy.controls['E_Photo']
  }

  get E_id(){
    return this.formEmploy.controls['E_id']
  }
}
