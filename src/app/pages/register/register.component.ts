import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthDataManegerService } from 'src/app/provider/auth-data-maneger.service';
import { AuthService } from 'src/app/provider/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  hide = true;
  hidee = true;
  register!: FormGroup
  errorMsg!: String;

  constructor(
    private fb: FormBuilder,
    private _authSerice: AuthService,
    private router: Router,
    private _authUser:AuthDataManegerService,
    private afs:AngularFirestore
    
  ) { }

  ngOnInit(): void {
    this.register = this.fb.group({
      name: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
      cpassword: ["", Validators.required],
      uid:[''],
    }, { validator: this.customvalidator() })
  }

  //get confirem password comtrol
  get confiremPassword() {
    return this.register.controls['cpassword']
  }

  get uid(){
    return this.register.controls['uid']
  }

  //customvalidator fuction for password and confim password
  customvalidator() {
    return (control: AbstractControl) => {
      if (control.get('password')?.value != control.get('cpassword')?.value) {
        return { 'notMatch': true }
      }
      return null
    }
  }

  // registation fuction
  registerMe() {

    if (this.register.valid) {
    
      this._authSerice.sinnUp(this.register.value).then((result) => {
       this.uid.setValue(result.user?.uid)
        this._authUser.setuser(result.user?.uid,this.register.value)

        this.afs.collection("user").doc(result.user?.uid).valueChanges().subscribe(data=>{
          localStorage.setItem("user",JSON.stringify(data))
        })
       
        this.router.navigateByUrl("login");
    
      }).catch((err) => {
    
        console.log(err)
    
        this.errorMsg = err.message
    
      });
    } else {
      this.errorMsg = "Enter details is not velid"
    }
  
  }



}
