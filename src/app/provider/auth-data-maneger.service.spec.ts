import { TestBed } from '@angular/core/testing';

import { AuthDataManegerService } from './auth-data-maneger.service';

describe('AuthDataManegerService', () => {
  let service: AuthDataManegerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthDataManegerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
