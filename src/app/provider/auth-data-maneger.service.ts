import { Injectable } from '@angular/core';
import { AngularFirestore , AngularFirestoreCollection } from "@angular/fire/firestore";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthDataManegerService {

  employCollection!:AngularFirestoreCollection
  user!:AngularFirestoreCollection

  constructor(private afs:AngularFirestore) {
    this.employCollection = this.afs.collection("employData")
    this.user = this.afs.collection('user')
   }

   setData(obj:any){
    return this.employCollection.add(obj)
   }

   setuser(id:any,obj:any){
    return this.user.doc(id).set(obj)
   }

  

   getdata(){
     return this.employCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
     )}

   delete(id:any){
    return this.employCollection.doc(id).delete()
   }

   update(id:any,obj:any){
     this.employCollection.doc(id).update(obj)
   }

   updateUser(id:any,obj:any){
     this.user.doc(id).update(obj)
   }
}
