import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {

  viewProfile:any=[]
  constructor(@Inject(MAT_DIALOG_DATA) public data:any) { 
    this.viewProfile = data
  }

  ngOnInit(): void {
  }

}
