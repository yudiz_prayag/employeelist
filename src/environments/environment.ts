// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyCmMZIMb9QH3bJ1V07TLsNCwc-NY25St7E",
    authDomain: "testproject-44.firebaseapp.com",
    databaseURL: "https://testproject-44-default-rtdb.firebaseio.com",
    projectId: "testproject-44",
    storageBucket: "testproject-44.appspot.com",
    messagingSenderId: "150226531718",
    appId: "1:150226531718:web:2726b4ab385484a655a807"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
