import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private auth:AngularFireAuth) { }

  loginWithGoogle(){
   return this.auth.signInWithPopup(new firebase.default.auth.GoogleAuthProvider())
  }

  sinnUp(obj:any){
    return this.auth.createUserWithEmailAndPassword(obj.email,obj.password)
    
  }

  signin(obj?:any){
    return this.auth.signInWithEmailAndPassword(obj.email,obj.password)
  }

  forgetPassword(obj:any){
    
    return this.auth.sendPasswordResetEmail(obj)

  }

  signout(){
    return this.auth.signOut()
  }

  
}

