import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthDataManegerService } from 'src/app/provider/auth-data-maneger.service';
import { AuthService } from 'src/app/provider/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  loginUser:any
  Edit=false
  editForm!:FormGroup



  constructor( private fb:FormBuilder,
    private _authdata:AuthDataManegerService,
    private router:Router,
    private _auth:AuthService
      
    ) { 
    const userdata = JSON.parse(localStorage.getItem("user")!)
    console.log(userdata)
    this.loginUser = userdata
  }

  ngOnInit(): void {
    this.editForm =  this.fb.group({
      name:[this.loginUser.name],
      email:[this.loginUser.email]
    })
  }



  edit(){
    this.Edit = !this.Edit
   
  }

  save(){
    

      this._authdata.updateUser(this.loginUser.uid,this.editForm.value)
      this._auth.signout()
      localStorage.removeItem("user")
      this.router.navigateByUrl("login")

  }
}
