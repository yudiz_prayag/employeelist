import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/provider/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ForgetComponent } from './forget/forget.component';
import { BehaviorSubject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup
  hide = true;
  errorMsg!: String;
  authsub: any = new BehaviorSubject(null);

  constructor(private fb: FormBuilder,
    private _authService: AuthService,
    private router: Router,
    private dialog: MatDialog,
    private afs:AngularFirestore
  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    
    })


  }


  // login function
  login() {

    if (this.loginForm.valid) {
      this._authService.signin(this.loginForm.value).then((result) => {
      //  console.log(result.user?.uid)
       this.afs.collection("user").doc(result.user?.uid).valueChanges().subscribe(data=>{
         
        localStorage.setItem("user",JSON.stringify(data))
       })
        this.router.navigateByUrl("/home");


       

        // this.authsub.next(result.user?.email)

      }).catch((err) => {

        console.log(err)
        
        this.errorMsg = err.message
      });



    } else {
      this.errorMsg = "Enter deatils is not velid"
    }
  }

  // login with google function
  googloLogin() {

    this._authService.loginWithGoogle().then((result) => {
    
      this.router.navigateByUrl("/home");
    
    }).catch((err) => {
    
      this.errorMsg = err.message
    
      console.log(err)

    });
  }

  // foget password function open dialog box
  forget() {
    this.dialog.open(ForgetComponent, { width: '300px', height: '200px', })
  }

}
