import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEmployComponent } from './pages/home/add-employ/add-employ.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AngularFireAuthGuard, hasCustomClaim, redirectUnauthorizedTo, redirectLoggedInTo, canActivate } from '@angular/fire/auth-guard';
import { UserProfileComponent } from './pages/header/user-profile/user-profile.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToItems = () => redirectLoggedInTo(['/home']);
const redirect = () => redirectLoggedInTo(['/user-profile']);


const routes: Routes = [
  {path:"",component:LoginComponent},

  {path:"login",component:LoginComponent ,...canActivate(redirectLoggedInToItems) },
  {path:"register",component:RegisterComponent},
  {path:"home",component:HomeComponent , canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  {path:"addemploy",component:AddEmployComponent},
  {path:"user-profile",component:UserProfileComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
